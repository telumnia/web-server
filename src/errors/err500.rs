use std::fmt;

pub struct Err500(String);

impl From<String> for Err500 {
    fn from(s: String) -> Self {
        Err500(s)
    }
}

impl fmt::Debug for Err500 {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}
