//! This module contains all that is related to the database. Two types
//! implement the `Repository` contract:
//! - `Implementation` that is the regular database manager,
//! - `Mock` that is a fake in-memory database, used for the tests.

mod implementation;
mod mock;
/// Used by the Diesel ORM to generate the needed code.
mod schema;

use crate::models::*;
use failure::Error;
use std::fmt::Debug;

pub trait Repository: Debug + Send + Sync {
    fn news(&self) -> Result<Vec<NewsArticle>, String>;

    fn news_article(&self, id: i32) -> Result<Option<NewsArticle>, String>;

    fn promos(&self) -> Result<Vec<Promo>, String>;

    fn one_promo(&self, id: i32) -> Result<Option<Promo>, String>;
}

pub type MyRepository = &'static dyn Repository; //Box<dyn Repository>;

pub fn mock() -> Result<MyRepository, !> {
    let repository = Box::new(mock::Mock::new());

    Ok(Box::leak(repository))
}

pub fn implementation(database_uri: String) -> Result<MyRepository, Error> {
    let repository = Box::new(implementation::Implementation::new(database_uri)?);

    Ok(Box::leak(repository))
}
