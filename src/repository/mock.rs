use super::Repository;
use crate::models::*;
use std::collections::HashMap;

#[derive(Debug)]
pub struct Mock {
    news: HashMap<i32, NewsArticle>,
    promos: HashMap<i32, Promo>,
}

macro_rules! insert {
    { $( $a:expr, )+ } => {{
        let mut map = HashMap::new();
        $(
            map.insert($a.id, $a);
        )+
        map
    }}
}

impl Mock {
    pub fn new() -> Self {
        let news = insert![
            NewsArticle {
                id: 0,
                title: "My first article".into(),
                content: "Lorem ipsum…".into(),
            },
            NewsArticle {
                id: 1,
                title: "My second article".into(),
                content: "Second article content".into(),
            },
            NewsArticle {
                id: 2,
                title: "My third article".into(),
                content: "Last article content".into(),
            },
        ];
        let promos = insert![
            Promo {
                id: 0,
                content: "A first promo".into(),
            },
            Promo {
                id: 1,
                content: "Another promo".into(),
            },
        ];

        Mock { news, promos }
    }
}

impl Repository for Mock {
    fn news(&self) -> Result<Vec<NewsArticle>, String> {
        Ok(self.news
            .iter()
            .map(|(_, article)| article.clone())
            .collect())
    }

    fn news_article(&self, id: i32) -> Result<Option<NewsArticle>, String> {
        Ok(self.news.get(&id).map(Clone::clone))
    }

    fn promos(&self) -> Result<Vec<Promo>, String> {
        Ok(self.promos
            .iter()
            .map(|(_, article)| article.clone())
            .collect())
    }

    fn one_promo(&self, id: i32) -> Result<Option<Promo>, String> {
        Ok(self.promos.get(&id).map(Clone::clone))
    }
}
