use super::schema::articles::dsl::articles as articles_;
use super::Repository;
use crate::models::*;
use diesel::mysql::MysqlConnection;
use diesel::r2d2::{ConnectionManager, Pool};
use diesel::{OptionalExtension, QueryDsl, RunQueryDsl};
use std::fmt;

type MysqlPool = Pool<ConnectionManager<MysqlConnection>>;

pub struct Implementation {
    sql_pool: MysqlPool,
}

impl Implementation {
    pub fn new(database_uri: String) -> Result<Self, r2d2::Error> {
        let manager = ConnectionManager::<MysqlConnection>::new(database_uri);
        let sql_pool = Pool::new(manager)?;

        Ok(Implementation { sql_pool })
    }
}

impl Repository for Implementation {
    fn news(&self) -> Result<Vec<NewsArticle>, String> {
        let connection = self
            .sql_pool
            .get()
            .map_err(|e| format!("Database error: {:?}", e))?;
        let results = articles_
            .load(&connection)
            .map_err(|e| format!("Database error: {:?}", e))?;

        Ok(results)
    }

    fn news_article(&self, id: i32) -> Result<Option<NewsArticle>, String> {
        let connection = self
            .sql_pool
            .get()
            .map_err(|e| format!("Database error: {:?}", e))?;
        let result = articles_
            .find(id)
            .get_result(&connection)
            .optional()
            .map_err(|e| format!("Database error: {:?}", e))?;

        Ok(result)
    }

    fn promos(&self) -> Result<Vec<Promo>, String> {
        unimplemented!()
    }

    fn one_promo(&self, id: i32) -> Result<Option<Promo>, String> {
        let _ = id;
        unimplemented!()
    }
}

impl fmt::Debug for Implementation {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Implementation {{ sql_pool }}")
    }
}
