#![allow(proc_macro_derive_resolution_fallback, unused_extern_crates)]

table! {
    articles (id) {
        id -> Integer,
        title -> Text,
        content -> Text,
    }
}

table! {
    promos (id) {
        id -> Integer,
        content -> Text,
    }
}
