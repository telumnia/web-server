//! This module is the heart of the server. The jobs must be done there. Most of
//! the time, the job is to get some data from the database through the
//! repository.

use crate::models::*;
use crate::repository::MyRepository;
use reqwest::{header, Client, Url};

#[derive(Debug)]
pub struct Service {
    repository: MyRepository,
    http_client: Client,
}

impl Service {
    pub fn new(repository: MyRepository) -> Self {
        let http_client = Client::new();

        Service {
            repository,
            http_client,
        }
    }

    pub fn echo<'a>(&self, message: &'a str) -> &'a str {
        message
    }

    pub fn news(&self) -> Result<Vec<NewsArticle>, String> {
        self.repository.news()
    }

    pub fn news_article(&self, id: i32) -> Result<Option<NewsArticle>, String> {
        self.repository.news_article(id)
    }

    pub fn promos(&self) -> Result<Vec<Promo>, String> {
        self.repository.promos()
    }

    pub fn one_promo(&self, id: i32) -> Result<Option<Promo>, String> {
        self.repository.one_promo(id)
    }

    pub fn add_user_to_group(
        &self,
        base_url: &Url,
        user: String,
        group: String,
    ) -> Result<(), &'static str> {
        //use reqwest::header::{HeaderMap, HeaderValue, USER_AGENT, CONTENT_TYPE};

        let payload = format!("{{ user: {}, group: {} }}", user, group);
        let result = self
            .http_client
            .post(base_url.join("/add_to_group").unwrap())
            .body(payload)
            .header(header::CONTENT_TYPE, "application/json")
            .send();

        match &result {
            Err(_) => Err("Cannot send to minecraft server"),
            Ok(response) if response.status().is_success() => Ok(()),
            Ok(_) => Err("Status error"),
        }
    }
}
