//! The controller of the server. It permits to manage the routes, but does
//! **not** do any business job. Each of those routing functions must only call
//! a service method and handle the HTTP stuff.

use crate::config::Config;
use crate::errors::Err500;
use crate::models::*;
use crate::service::Service;
use rocket::http::{ContentType, RawStr, Status};
use rocket::response::{NamedFile, Redirect};
use rocket::{get, put, routes};
use rocket::{Request, Response, Route, State};
use rocket_contrib::json::Json;
use std::path::PathBuf;

/// Static files server.
#[get("/<path..>", rank = 4)]
pub fn serve(config: State<'_, Config>, path: PathBuf) -> Option<NamedFile> {
    NamedFile::open(config.client_path.join(path)).ok()
}

/// Redirects *GET /* to *GET /index.html*.
#[get("/")]
pub fn index() -> Redirect {
    Redirect::to("/index.html")
}

/// Used by the fronted.
pub mod www {
    use super::*;

    #[get("/echo/<message>")]
    fn echo<'a>(service: State<'_, Service>, message: &'a RawStr) -> &'a str {
        service.echo(message)
    }

    #[get("/news")]
    fn news(service: State<'_, Service>) -> Result<Json<Vec<NewsArticle>>, Err500> {
        Ok(Json(service.news()?))
        //Ok(Json(service.news().map_err(err500)?))
    }

    #[get("/news/<id>")]
    fn news_article(
        service: State<'_, Service>,
        id: i32,
    ) -> Result<Option<Json<NewsArticle>>, Err500> {
        Ok(service.news_article(id)?.map(Json))
    }

    #[get("/promos")]
    fn promos(service: State<'_, Service>) -> Result<Json<Vec<Promo>>, Err500> {
        Ok(Json(service.promos()?))
    }

    #[get("/promos/<id>")]
    fn one_promo(service: State<'_, Service>, id: i32) -> Result<Option<Json<Promo>>, Err500> {
        Ok(service.one_promo(id)?.map(Json))
    }

    pub fn routes() -> Vec<Route> {
        routes![echo, news, news_article, promos, one_promo]
    }
}

/// Call to the Minecraft server API.
pub mod api {
    use super::*;

    #[put("/add/<user>/to/<group>")]
    fn add_user_to_group(
        config: State<'_, Config>,
        service: State<'_, Service>,
        user: String,
        group: String,
    ) -> Result<(), Status> {
        service
            .add_user_to_group(&config.minecraft_server, user, group)
            .map_err(|reason| Status {
                reason,
                ..Status::InternalServerError
            })
    }

    pub fn routes() -> Vec<Route> {
        routes![add_user_to_group]
    }
}

fn err500<'r>(s: String) -> rocket::Response<'r> {
    let body = format!(
        "\
        <!DOCTYPE html>\
        <html>\
        <head>\
            <meta charset=\"utf-8\">\
            <title>500 Internal Server Error</title>\
        </head>\
        <body align=\"center\">\
            <div align=\"center\">\
                <h1>500: Internal Server Error</h1>\
                <p>The server encountered an internal error\
            while processing this request:</p>\
                <p>{}</p>
                <hr />\
                <small>Rocket</small>\
            </div>\
        </body>\
        </html>",
        s
    );
    rocket::Response::build()
        .status(Status::InternalServerError)
        .header(ContentType::HTML)
        .sized_body(std::io::Cursor::new(body))
        .finalize()
}
