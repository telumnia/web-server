//! This module handles the custom configuration from the `Rocket.toml`
//! configuration file.

use failure::{Error, ResultExt};
use reqwest::Url;
use rocket::config::Config as RocketConfig;
use std::path::PathBuf;

#[derive(Debug)]
pub struct Config {
    pub client_path: PathBuf,
    pub database_uri: Option<String>,
    pub minecraft_server: Url,
}

impl Config {
    pub fn new(config: &RocketConfig, mock: bool) -> Result<Self, Error> {
        let minecraft_server = config
            .get_str("minecraft_server")
            .context("\t> Cannot get the field 'minecraft_server'")?
            .parse::<Url>()
            .context("\t> Cannot be parsed into an Url")?;
        let client_path = config
            .get_str("client_path")
            .map(Into::into)
            .context("\t> Cannot get the field 'client_path'")?;
        let database_uri = if mock {
            None
        } else {
            config.get_str("database_uri").ok().map(Into::into)
        };

        Ok(Config {
            client_path,
            database_uri,
            minecraft_server,
        })
    }
}
