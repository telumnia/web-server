#![feature(never_type)]
#![feature(proc_macro_hygiene, decl_macro)]
#![forbid(missing_debug_implementations)]
#![deny(rust_2018_idioms)]

#[macro_use]
extern crate diesel;

mod config;
mod errors;
mod models;
mod repository;
mod routes;
mod service;

use self::routes::*;
use crate::{config::Config, service::Service};
use failure::{Error, ResultExt};
use rocket::routes;
use std::env::args;

fn main() -> Result<!, Error> {
    let rocket = rocket::ignite();
    let mock = match &args().nth(1) {
        None => false,
        Some(first) if first == "--mock" => true,
        _ => panic!("Usage: server [--mock]\n\n\t--mock: mock the database."),
    };
    let config = Config::new(rocket.config(), mock).context("\t> Cannot get the configuration")?;
    let repository = match &config.database_uri {
        Some(database_uri) => repository::implementation(database_uri.clone())
            .context("Cannot create a repository")?,
        None => repository::mock()?,
    };
    let launch_error = rocket
        .manage(Service::new(repository))
        .manage(config)
        .mount("/", routes![serve, index])
        .mount("/www", www::routes())
        .mount("/api", api::routes())
        .launch();

    Err(launch_error.into())
}
