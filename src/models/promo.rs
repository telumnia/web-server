#![allow(proc_macro_derive_resolution_fallback)]

use serde_derive::Serialize;

#[derive(Debug, Clone, Serialize, Queryable)]
pub struct Promo {
    pub id: i32,
    pub content: String,
}
