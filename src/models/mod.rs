//! All the models that will be used in the application. These are DTOs.

mod news_article;
pub use self::news_article::NewsArticle;

mod promo;
pub use self::promo::Promo;
