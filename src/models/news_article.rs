#![allow(proc_macro_derive_resolution_fallback)]

use serde_derive::Serialize;

#[derive(Debug, Clone, Serialize, Queryable)]
pub struct NewsArticle {
    pub id: i32,
    pub title: String,
    pub content: String,
}
