create table articles (
    id int key auto_increment,
    title varchar(128),
    content varchar(2048)
);

create table promos (
    id int key auto_increment,
    content varchar(2048)
);
