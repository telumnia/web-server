Web server and public API of Telumnia project.

# Documentation

The 3 main modules are, in the order of request treatment:

- `routes`, that handles the HTTP requests and forwards them to the service,
- `service`, that does the job, and possibly forwards it to the repository,
- `repository`, that manage the databases.

# Todo

See the [board](https://gitlab.com/telumnia/web-server/boards).
